package optimize.problems;

import optimize.core.Problem;
import optimize.util.DataIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mario on 9/03/14.
 */
public class KnapsackProblem implements Problem {
    private int[] values;
    private int[] weights;
    private int capacity;
    private int numItems;


    public KnapsackProblem(String filename) throws IOException {
        // read the lines out of the file
        List<String> lines = new ArrayList<String>();

        try (BufferedReader input = DataIO.getBufferedReader("data/knapsack" + filename)){
            String line = null;
            while ((line = input.readLine()) != null) {
                lines.add(line);
            }
        }


        // parse the data in the file
        String[] firstLine = lines.get(0).split("\\s+");
        numItems = Integer.parseInt(firstLine[0]);
        capacity = Integer.parseInt(firstLine[1]);

        values = new int[numItems];
        weights = new int[numItems];

        for (int i = 1; i < numItems + 1; i++) {
            String line = lines.get(i);
            String[] parts = line.split("\\s+");

            values[i - 1] = Integer.parseInt(parts[0]);
            weights[i - 1] = Integer.parseInt(parts[1]);
        }
    }

    public int[] getValues() {
        return values;
    }

    public int[] getWeights() {
        return weights;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getNumItems() {
        return numItems;
    }

}
