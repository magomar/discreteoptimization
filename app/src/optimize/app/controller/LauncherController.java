package optimize.app.controller;

import optimize.app.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.stage.DirectoryChooser;
import optimize.core.Problem;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Mario Gómez Martínez <magomar@gmail.com>
 */
public class LauncherController implements Initializable {
    @FXML
    private ComboBox<Problem> problems;
    @FXML
    private ListView problemsFound;
    private ObservableList<File> datasetsModel = FXCollections.observableArrayList();

    @FXML

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        problemsFound.setItems(datasetsModel);
    }

    @FXML
    private void refreshDatasets() {
//        String directoryName = libraryDirectoryTextField.getText();
        String directoryName = null; // TODO
        if (!directoryName.isEmpty()) {
            File mainDirectory = new File(directoryName);
            String[] directories = mainDirectory.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return  new File(dir, name).isDirectory();
                }
            });
            datasetsModel.clear();
            for (int i = 0; i < directories.length; i++) {
                String directory = directories[i];
                datasetsModel.add(new File(directory));
            }
        }
    }

    @FXML
    private void selectLibraryDirectoryAction(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle("Chose a directory containing datasets");
        File defaultDirectory = new File(System.getProperty("user.dir") + File.separator + Main.DATA_FOLDER);
        directoryChooser.setInitialDirectory(defaultDirectory);
        //Show open file dialog
        File file = directoryChooser.showDialog(null);

        if (file != null) {
//            libraryDirectoryTextField.setText(file.getPath());
            // TODO
            refreshDatasets();
        }

    }


    public void handleImportDatasetButtonAction(ActionEvent actionEvent) {
    }

}

