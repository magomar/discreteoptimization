package optimize.solvers;

import optimize.core.Problem;
import optimize.core.Service;

/**
 * Created by Mario on 9/03/14.
 */
public interface Solver<P extends Problem> extends Service {
    void solve(P problem);
    ProblemType getProblemType();

    public enum ProblemType {
        KNAPSACK("Knapsack Problem"),
        COLORING("Graph Coloring Problem");

        ProblemType(String name) {
            this.name = name;
        }

        private final String name;

        public String getName() {
            return name;
        }
    }
}
