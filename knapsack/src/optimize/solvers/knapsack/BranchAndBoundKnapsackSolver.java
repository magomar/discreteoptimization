package optimize.solvers.knapsack;

import optimize.problems.KnapsackProblem;
import optimize.solvers.KnapsackSolver;

/**
 * The class <code>optimize.optimize.algorithms.knapsack.algorithms.GreedyKnapsackSolver</code> is an implementation of a Brand & Bound algorithm to solve the optimize.optimize.algorithms.knapsack problem.
 */
public class BranchAndBoundKnapsackSolver implements KnapsackSolver {

    /**
     * Read the instance, solve it, and print the solution in the standard output
     */
    @Override
    public void solve(KnapsackProblem problem) {
    }

    @Override
    public ProblemType getProblemType() {
        return ProblemType.KNAPSACK;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}