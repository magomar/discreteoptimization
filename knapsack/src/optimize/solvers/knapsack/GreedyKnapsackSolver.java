package optimize.solvers.knapsack;

import optimize.core.Problem;
import optimize.problems.KnapsackProblem;
import optimize.solvers.KnapsackSolver;

/**
 * The class <code>optimize.optimize.algorithms.knapsack.algorithms.GreedyKnapsackSolver</code> is an implementation of a greedy algorithm to solve the optimize.optimize.algorithms.knapsack problem.
 */
public class GreedyKnapsackSolver implements KnapsackSolver {
    private int[] values;
    private int[] weights;
    private int capacity;
    private int numItems;


    /**
     * Read the instance, solve it, and print the solution in the standard output
     */
    @Override
    public void solve(KnapsackProblem problem)  {
        values = problem.getValues();
        weights = problem.getWeights();
        capacity = problem.getCapacity();
        numItems = problem.getNumItems();

        // a trivial greedy algorithm for filling the optimize.optimize.algorithms.knapsack
        // it takes items in-order until the optimize.optimize.algorithms.knapsack is full
        int value = 0;
        int weight = 0;
        int[] taken = new int[numItems];

        for (int i = 0; i < numItems; i++) {
            if (weight + weights[i] <= capacity) {
                taken[i] = 1;
                value += values[i];
                weight += weights[i];
            } else {
                taken[i] = 0;
            }
        }

        // prepare the solution in the specified output format
        System.out.println(value + " 0");
        for (int i = 0; i < numItems; i++) {
            System.out.print(taken[i] + " ");
        }
        System.out.println("");
    }

    @Override
    public ProblemType getProblemType() {
        return ProblemType.KNAPSACK;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}