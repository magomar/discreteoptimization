package optimize.solvers;

import optimize.problems.KnapsackProblem;

/**
 * Created by Mario on 9/03/14.
 */
public interface KnapsackSolver extends Solver<KnapsackProblem> {
}
