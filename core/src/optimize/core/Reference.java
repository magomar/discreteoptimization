package optimize.core;

/**
 * Created by Mario on 9/03/14.
 */
public class Reference<S extends Service> {

    private S object;
    private final String name;

    public Reference(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean hasObject() {
        return object != null;
    }

    public S getObject() {
        return object;
    }

    public void setObject(final S object) {
        this.object = object;
    }

}
