import optimize.core.Problem;
import optimize.solvers.KnapsackSolver;

import java.util.ServiceLoader;

/**
 * Created by Mario on 9/03/14.
 */
public class TestServiceLoader {
    public static void main(String[] args) {
        ServiceLoader<Problem> problems= ServiceLoader.load(Problem.class);
        ServiceLoader<KnapsackSolver> knapsackSolvers= ServiceLoader.load(KnapsackSolver.class);
        if (knapsackSolvers.iterator().hasNext()) {
            for (KnapsackSolver knapsackSolver : knapsackSolvers) {
                System.out.println(knapsackSolver.getName() + " for the " + knapsackSolver.getProblemType().getName());
            }
        } else {
            System.out.println("No solver found !");
        }
    }


}
