package optimize.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gómez Martínez <magomar@gmail.com>
 */
public class DataIO {

    public static BufferedReader getBufferedReader(String fileName) throws IOException {
        Path path = FileSystems.getDefault().getPath(fileName);
        BufferedReader bufferedReader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        return bufferedReader;
    }

}
